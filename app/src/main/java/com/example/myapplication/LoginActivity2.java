package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class LoginActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);
        WebView login = findViewById(R.id.login);
        WebSettings settings=login.getSettings();
        //允许操心js
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        //允许操作DOM
        settings.setDomStorageEnabled(true);
        login.addJavascriptInterface(new LoginScript(),"login");
        login.loadUrl("file:///android_asset/login.html");

    }


    public class LoginScript{
        @JavascriptInterface
        public void toOrderPage(){
            Intent intent=new Intent();
            intent.setClass(LoginActivity2.this,OrderActivity2.class);
            startActivity(intent);
            finish();

        }
    }
}