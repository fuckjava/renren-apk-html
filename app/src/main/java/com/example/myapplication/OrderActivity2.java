package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.alipay.sdk.app.EnvUtils;
import com.alipay.sdk.app.PayTask;

import org.json.JSONObject;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class OrderActivity2 extends AppCompatActivity {
    private WebView orderView;

    @Override
    protected void onResume() {
        //挂起app，然后切回当前app调用的方法
        super.onResume();
        OrderActivity2.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                orderView.loadUrl("file:///android_asset/order.html");
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //沙箱环境
        EnvUtils.setEnv(EnvUtils.EnvEnum.SANDBOX);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order2);

        orderView = findViewById(R.id.order);
        WebSettings settings = orderView.getSettings();
        //允许操心js
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        //允许操作DOM
        settings.setDomStorageEnabled(true);
        orderView.addJavascriptInterface(new OrderScript(),"order");
        orderView.loadUrl("file:///android_asset/order.html");
    }

    public class OrderScript{
        @JavascriptInterface
        public void pay(String orderString,String token,String orderId){
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    PayTask payTask = new PayTask(OrderActivity2.this);
                    Map<String, String> result = payTask.payV2(orderString, true);
//                    System.out.println(result);
                    String resultStatus=result.get("resultStatus");
                    if(resultStatus.equals("9000")){
                        String path="http://http://192.168.0.4:8282/renren-fast/app/zfb/updateOrderStatus";
                        try{
                            URL url=new URL(path);
                            HttpURLConnection con= (HttpURLConnection) url.openConnection();
                            con.setConnectTimeout(5000);
                            con.setRequestMethod("POST");
                            JSONObject json=new JSONObject();
                            json.put("orderId",orderId);
                            con.setRequestProperty("Content-Type","application/json");
                            con.setRequestProperty("token",token);
                            con.setDoOutput(true);
                            OutputStream out=con.getOutputStream();
                            out.write(json.toString().getBytes());
                            out.close();
                            if(con.getResponseCode()==200){
                                OrderActivity2.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        orderView.loadUrl("file:///android_asset/order.html");
                                    }
                                });
                            }
                            con.disconnect();
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                }
            };

            Thread thread = new Thread(runnable);
            thread.start();
        }

        @JavascriptInterface
        public void wapPay(String code) {

            OrderActivity2.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Intent intent=new Intent();
                    intent.setAction("android.intent.action.VIEW");
                    String url="http://192.168.0.4:8282/renren-fast/app/unionpay/htmlCodePage?code="+code;
                    Uri uri= Uri.parse(url);
                    intent.setData(uri);
                    startActivity(intent);
                }
            });
        }
    }
}